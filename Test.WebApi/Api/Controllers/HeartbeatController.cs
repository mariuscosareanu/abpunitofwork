﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using Test.Dto;
using Test.Services;
using Test.Model;

namespace Test.Api.Controllers
{
    [AllowAnonymous]
    public class HeartbeatController : ApiController
    {
        private readonly ISessionsService _sessionsService;

        public HeartbeatController(ISessionsService sessionsService)
        {
            _sessionsService = sessionsService;
        }

        [HttpGet]
        public async Task<IHttpActionResult> Submit()
        {
            for (var i = 1; i <= 4; i++)
            {
                var rnd = new Random();

                await _sessionsService.UpdateSession(new SessionDto
                {
                    Id = i,
                    State = (SessionState)rnd.Next(1, 4)
                });

                await Task.Delay(5 * 1000);
            }

            return Ok();
        }
    }
}
