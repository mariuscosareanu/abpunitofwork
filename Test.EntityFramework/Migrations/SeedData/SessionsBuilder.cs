using System;
using Test.EntityFramework;
using Test.Model;

namespace Test.Migrations.SeedData
{
    public class SessionsBuilder
    {
        private readonly TestDbContext _context;

        public SessionsBuilder(TestDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSessions();
        }

        private void CreateSessions()
        {
            //add four sessions
            _context.Sessions.Add(new Session
            {
                ReportingTime = DateTime.UtcNow,
                State = SessionState.Available
            });

            _context.Sessions.Add(new Session
            {
                ReportingTime = DateTime.UtcNow,
                State = SessionState.Available
            });

            _context.Sessions.Add(new Session
            {
                ReportingTime = DateTime.UtcNow,
                State = SessionState.Available
            });

            _context.Sessions.Add(new Session
            {
                ReportingTime = DateTime.UtcNow,
                State = SessionState.Available
            });
        }
    }
}