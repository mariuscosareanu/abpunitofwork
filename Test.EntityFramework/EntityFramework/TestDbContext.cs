﻿using System.Data.Common;
using System.Data.Entity;
using Abp.Zero.EntityFramework;
using Test.Authorization.Roles;
using Test.MultiTenancy;
using Test.Users;
using Test.Model;

namespace Test.EntityFramework
{
    public class TestDbContext : AbpZeroDbContext<Tenant, Role, User>
    {
        public IDbSet<Session> Sessions { get; set; }

        /* NOTE: 
         *   Setting "Default" to base class helps us when working migration commands on Package Manager Console.
         *   But it may cause problems when working Migrate.exe of EF. If you will apply migrations on command line, do not
         *   pass connection string name to base classes. ABP works either way.
         */
        public TestDbContext()
            : base("Default")
        {

        }

        /* NOTE:
         *   This constructor is used by ABP to pass connection string defined in TestDataModule.PreInitialize.
         *   Notice that, actually you will not directly create an instance of TestDbContext since ABP automatically handles it.
         */
        public TestDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }

        //This constructor is used in tests
        public TestDbContext(DbConnection connection)
            : base(connection, true)
        {

        }
    }
}
