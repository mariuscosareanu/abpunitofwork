﻿using System.Reflection;
using Abp.AutoMapper;
using Abp.Modules;
using Castle.MicroKernel.Registration;
using Test.Services;

namespace Test
{
    [DependsOn(typeof(TestCoreModule), typeof(AbpAutoMapperModule))]
    public class TestApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Modules.AbpAutoMapper().Configurators.Add(mapper =>
            {
                //Add your custom AutoMapper mappings here...
                //mapper.CreateMap<,>()
            });
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
            IocManager.IocContainer.Register(Component.For<ISessionsService>()
                                                               .ImplementedBy<SessionService>()
                                                               .Named("MySessionService")
                                                               .IsDefault()
                                                               .LifestyleTransient());
        }
    }
}
