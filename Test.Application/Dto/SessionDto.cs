﻿using Abp.Application.Services.Dto;
using Test.Model;

namespace Test.Dto
{
    public class SessionDto : EntityDto<long>
    {
        public SessionState State { get; set; }
    }
}
