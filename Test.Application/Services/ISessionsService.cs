﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Domain.Repositories;
using Test.Dto;
using Test.Model;

namespace Test.Services
{
    public interface ISessionsService : IApplicationService
    {
        Task UpdateSession(SessionDto session);
    }

    public class SessionService : ApplicationService, ISessionsService
    {
        private readonly IRepository<Session, long> _repository;

        public SessionService(IRepository<Session, long> repository)
        {
            _repository = repository;
        }
  

        public async Task UpdateSession(SessionDto session)
        {
            var poco = await _repository.FirstOrDefaultAsync(s => s.Id == session.Id);
            poco.State = session.State;
            poco.ReportingTime = DateTime.UtcNow;

            await UnitOfWorkManager.Current.SaveChangesAsync();
        }
    }
}
