﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;

namespace Test.Model
{
    public class Session : Entity<long>
    {
        [Required]
        [Column("State")]
        public SessionState State { get; set; }

        [Required]
        public DateTime ReportingTime { get; set; }
    }

    public enum SessionState
    {
        Available,
        Busy,
        Offline
    }
}
